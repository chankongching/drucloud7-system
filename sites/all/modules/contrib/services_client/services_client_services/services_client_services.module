<?php

/**
 * @file
 * Provides services that can be installed on remote site which makes configuring
 * events easier.
 */

/**
 * Implements hook_services_resources().
 */
function services_client_services_services_resources() {
  $resources = array(
    'services_client' => array(
      'actions' => array(
        'entity_info' => array(
          'help' => 'Describe entities, properties and fields available on system',
          'file' => array('type' => 'inc', 'module' => 'services_client_services', 'name' => 'services_client_services'),
          'callback' => '_services_client_services_entity_info',
          'access callback' => '_services_client_services_info_access',
          'access arguments' => array('entity_info'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'entity_type',
              'optional' => TRUE,
              'source' => array('path' => 1),
              'type' => 'string',
              'description' => 'Optionally provide entity info',
            ),
          ),
        ),
      ),
    ),
  );

  return $resources;
}

/**
 * Implements hook_services_resources_alter();
 */
function services_client_services_services_resources_alter(&$resources, $endpoint) {
  if (isset($resources['user']['actions']) && empty($resources['user']['actions']['list_roles'])) {
    $resources['user']['actions']['list_roles'] = array(
      'help' => 'List all user roles',
      'callback' => '_servics_client_services_user_list_roles',
      'access callback' => '_servics_client_services_user_access',
      'access arguments' => array('list_roles'),
      'file' => array('type' => 'inc', 'module' => 'services_client_services', 'name' => 'services_client_services'),
    );
  }
}

/**
 * Implements hook_services_entity_resource_info().
 */
function services_client_services_services_entity_resource_info() {
  $result['services_client_uuid'] = array(
    'title' => 'Services Client UUID Entity Processor',
    'description' => 'Acts as a generic Services Client wrapper for entities which can be controlled by UUID rather than local ID. Data structures are exactly what they are in Drupal.',
    'class' => 'ServicesClientUUIDResourceController',
  );

  return $result;
}

/**
 * Implements hook_menu().
 */
function services_client_services_menu() {
  $items = array();

  $items['admin/config/services/services-client-uuid-entity'] = array(
    'title' => 'Services Client UUID Entity settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('services_client_services_settings'),
    'access arguments' => array('administer services'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Form callback;
 *
 * Allow to configure which entity types should be.
 * handled by Acquia Services Entity module
 */
function services_client_services_settings($form, &$form_state) {
  $form = array();

  $options = array_map(function ($item) {
    return $item['label'];
  }, entity_get_info());

  asort($options);

  $form['services_client_services_uuid_entities'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Entities'),
    '#description' => t('Select entities that should be handled by Acquia Services Entity module.'),
    '#options' => $options,
    '#default_value' => variable_get('services_client_services_uuid_entities', array()),
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'services_client_services_settings_submit';
  return $form;
}

/**
 * Form submit; Rebuild entity info.
 */
function services_client_services_settings_submit($form, &$form_state) {
  entity_info_cache_clear();
}

/**
 * Implements hook_entity_info_alter().
 */
function services_client_services_entity_info_alter(&$entity_info) {
  $entity_types = array_filter(variable_get('services_client_services_uuid_entities', array()));
  foreach ($entity_types as $entity_type) {
    $entity_info[$entity_type]['resource controller'] = 'ServicesClientUUIDResourceController';
  }
}
