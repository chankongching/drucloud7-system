<?php
/**
 * @file
 * drucloud_corporate_pages.entityqueue_default.inc
 */

/**
 * Implements hook_entityqueue_default_queues().
 */
function drucloud_corporate_pages_entityqueue_default_queues() {
  $export = array();

  $queue = new EntityQueue();
  $queue->disabled = FALSE; /* Edit this to true to make a default queue disabled initially */
  $queue->api_version = 1;
  $queue->name = 'featured_on';
  $queue->label = 'Featured on';
  $queue->language = 'en';
  $queue->handler = 'simple';
  $queue->target_type = 'taxonomy_term';
  $queue->settings = array(
    'target_bundles' => array(
      'media' => 'media',
    ),
    'min_size' => '0',
    'max_size' => '4',
  );
  $export['featured_on'] = $queue;

  return $export;
}
