<?php
/**
 * @file
 * drucloud_corporate_pages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function drucloud_corporate_pages_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "entityqueue" && $api == "entityqueue_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function drucloud_corporate_pages_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
