<?php
/**
 * @file
 * Code for the drucloudtagroll feature.
 */

include_once 'drucloud_tagroll.features.inc';

define('COCONUTS_TAGROLL_TAGS_VOCABULARY_MACHINE_NAME', 'tags');
define('COCONUTS_TAGROLL_CACHE_EXPIRE', 20 * 60);

/**
 * Implements hook_menu().
 */
function drucloud_tagroll_menu() {
  $items['drucloud-tagroll/teasers-view/%taxonomy_term'] = array(
    'page callback' => 'drucloud_tagroll_teasers_view',
    'page arguments' => array(2),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['drucloud-tagroll/teasers-view/%taxonomy_term/%taxonomy_term'] = array(
    'page callback' => 'drucloud_tagroll_teasers_view',
    'page arguments' => array(2, 3),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_views_pre_render().
 */
function drucloud_tagroll_views_pre_render(&$vars) {
  if ($vars->name == 'tagroll') {
    drupal_add_js(drupal_get_path('module', 'drucloud_tagroll') . '/js/drucloud_tagroll.js');
  }
}

/**
 * Build view.
 */
function drucloud_tagroll_get_view($primary_term, $secondary_term, $view_name, $display_id) {
  $view = views_get_view($view_name);
  $view->set_display($display_id);
  $view->set_use_ajax(TRUE);

  // Clear filters.
  drucloud_tagroll_clear_term_filter($view, 'field_tags_tid', $display_id);

  // Add filters.
  if ($primary_term) {
    drucloud_tagroll_set_term_filter($view, 'field_tags_tid', $display_id, $primary_term->tid);
  }

  if ($secondary_term) {
    drucloud_tagroll_set_term_filter($view, 'field_tags_tid', $display_id, $secondary_term->tid);
  }

  return $view;
}

/**
 * Clear filters to view.
 */
function drucloud_tagroll_clear_term_filter(&$view, $field_name, $display_id) {
  $filter = $view->get_item($display_id, 'filter', $field_name);
  $filter['value'] = array();
  $view->set_item($display_id, 'filter', $field_name, $filter);
}

/**
 * Set tags filters to view.
 */
function drucloud_tagroll_set_term_filter(&$view, $field_name, $display_id, $value) {
  $filter = $view->get_item($display_id, 'filter', $field_name);
  $filter['value'][] = $value;
  $view->set_item($display_id, 'filter', $field_name, $filter);
}

/**
 * Menu callback for teasers view.
 */
function drucloud_tagroll_teasers_view($primary_term = FALSE, $secondary_term = FALSE) {
  // Get cache name.
  $name = drucloud_tagroll_cache_name('drucloud_tagroll_teasers', $primary_term, $secondary_term);

  // Check cache.
  $cache = cache_get($name, 'cache');
  if ($cache) {
    $output = $cache->data;
  }
  else {
    $view = drucloud_tagroll_get_view($primary_term, $secondary_term, 'menu_teasers', 'menu_teasers');
    $view->pre_execute();
    $view->execute();
    $output = $view->render();
    cache_set($name, $output, 'cache', time() + COCONUTS_TAGROLL_CACHE_EXPIRE);
  }

  echo $output;
  exit(0);
}

/**
 * Return cache name.
 */
function drucloud_tagroll_cache_name($type, $primary_term = FALSE, $secondary_term = FALSE) {
  $name = $type;
  if ($primary_term == TRUE) {
    $name .= '_' . $primary_term->tid;
  }
  if ($secondary_term) {
    $name .= '_' . $secondary_term->tid;
  }

  return $name;
}
