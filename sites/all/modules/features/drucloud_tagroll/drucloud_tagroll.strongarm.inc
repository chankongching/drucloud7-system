<?php
/**
 * @file
 * drucloud_tagroll.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function drucloud_tagroll_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'xmlsitemap_settings_taxonomy_term_tags';
  $strongarm->value = array(
    'status' => '1',
    'priority' => '0.5',
  );
  $export['xmlsitemap_settings_taxonomy_term_tags'] = $strongarm;

  return $export;
}
