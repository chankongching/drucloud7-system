<?php

/**
 * @file
 * Handles incoming requests to fire off regularly-scheduled tasks (cron jobs).
 */

/**
 * Root directory of Drupal installation.
 */
define('DRUPAL_ROOT', getcwd());

include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
// Use the database we set up earlier
db_set_active('old');
$result = db_query('SELECT f.*, c.title FROM {field_data_field_image} n LEFT JOIN {file_managed} f on f.fid = n.field_image_fid LEFT JOIN {node} c on c.nid = n.entity_id WHERE n.bundle = :uid', array(':uid' => 'article'));
// Result is returned as a iterable object that returns a stdClass object on each iteration
db_set_active();
foreach ($result as $record) {
  $node->field_image = array(
    'und' => array(
      0 => array(
        'fid' => $record->fid,
        'filename' => $record->filename,
        'filemime' => $record->filemime,
        'uid' => 1,
        'uri' => 's3://field/image/' . $record->filename,
        'status' => 1,
        'display' => 1
      )
    )
  );
  $result = db_query("SELECT n.nid FROM {node} n WHERE n.title = :title AND n.type = :type", array(":title"=> $record->title, ":type"=> 'article'));  
  $nid = $result->fetchField();
  $newnode = node_load(48475);
  echo '<pre>';
  var_dump($newnode);
  echo '</pre>';
  exit();
}

// Go back to the default database,
// otherwise Drupal will not be able to access its own data later on.
db_set_active();
$node = node_load(48177);
echo '<pre>';
var_dump($node);
echo '</pre>';
exit();
